from jira import JIRA
import sys

def create_ticket(summary, description, project_key, issue_type):
    jira_options = {'server': 'https://your-jira-instance.atlassian.net'}
    jira = JIRA(options=jira_options, basic_auth=('your-email@example.com', 'your-api-token'))
    
    issue_dict = {
        'project': {'key': project_key},
        'summary': summary,
        'description': description,
        'issuetype': {'name': issue_type},
    }
    
    try:
        new_issue = jira.create_issue(fields=issue_dict)
        print(f"Ticket created successfully: {new_issue.key}")
        return new_issue.key
    except Exception as e:
        print(f"Failed to create ticket: {str(e)}")
        return None

if __name__ == "__main__":
    create_ticket('System Anomaly Detected', 'High CPU usage detected on production server.', 'IT', 'Bug')
