from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

def send_slack_message(channel, message):
    client = WebClient(token='your-slack-token')
    try:
        response = client.chat_postMessage(channel=channel, text=message)
        print(f"Message sent to {channel}: {response['message']['text']}")
    except SlackApiError as e:
        print(f"Failed to send message: {e.response['error']}")

if __name__ == "__main__":
    send_slack_message('#incident-alerts', 'This is a test alert from the Incident Response System.')
