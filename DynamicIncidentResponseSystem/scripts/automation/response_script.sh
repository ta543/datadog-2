#!/bin/bash

# Set Variables
INSTANCE_ID="i-0abcd1234efgh5678"
IMAGE_ID="ami-12345678"
INSTANCE_TYPE="t2.medium"
KEY_NAME="your-key-pair"
SECURITY_GROUP="sg-123abc45"
SUBNET_ID="subnet-6789def0"
TAG="ResourceType=instance,Tags=[{Key=Project,Value=DDAutoScale}]"

# Increase EC2 instance count
echo "Scaling up EC2 instances..."
aws ec2 run-instances --image-id $IMAGE_ID --count 1 --instance-type $INSTANCE_TYPE --key-name $KEY_NAME --security-group-ids $SECURITY_GROUP --subnet-id $SUBNET_ID --tag-specifications $TAG
echo "Scale up initiated."

# Notify via DataDog (Assuming DataDog CLI or API integration setup)
# Replace with actual DataDog command or API call to log event
echo "Sending notification to DataDog..."
# Placeholder for DataDog notification command
echo "Notification sent."
