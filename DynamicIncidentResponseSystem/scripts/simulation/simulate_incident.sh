#!/bin/bash

# Set Variables
INSTANCE_ID="i-0abcd1234efgh5678"

# Simulate high CPU load
echo "Simulating high CPU load on instance $INSTANCE_ID..."
aws ec2 send-command --instance-ids $INSTANCE_ID --document-name "AWS-RunShellScript" --parameters commands=["stress --cpu 8 --timeout 600"]
echo "High CPU load simulation initiated."
